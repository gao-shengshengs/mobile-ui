const fs = require('fs')

fs.readdir('./images', function (err, files) {
  let emojiStr = 'export default {\n'
  for (let i = 0; i < files.length; i++) {
    const file = files[i]
    const bitmap = fs.readFileSync('./images/' + file)
    const base64Str = Buffer.from(bitmap, 'binary').toString('base64')
    emojiStr += `  '${file.split('.')[0]}': '${'data:image/png;base64,' + base64Str}',\n`
  }
  fs.writeFileSync('emojiImages.js', `${emojiStr}}`)
})
