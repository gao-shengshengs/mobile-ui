import { Foundation } from '@/ui-utils'

/**
 * 金钱单位置换  2999 --> 2,999.00
 * @param val
 * @param unit
 * @param location
 * @returns {*}
 */
export function unitPrice(val, unit, location) {
  if (!val) val = 0
  let price = Foundation.formatPrice(val)
  if (location === 'before') {
    return price.substr(0, price.length - 3)
  }
  if (location === 'after') {
    return price.substr(-2)
  }
  return (unit || '') + price
}

/**
 * 处理unix时间戳，转换为可阅读时间格式
 * @param unix
 * @param format
 * @returns {*|string}
 */
export function unixToDate(unix, format) {
  let _format = format || 'yyyy-MM-dd hh:mm:ss'
  const d = new Date(unix * 1000)
  const o = {
    'M+': d.getMonth() + 1,
    'd+': d.getDate(),
    'h+': d.getHours(),
    'm+': d.getMinutes(),
    's+': d.getSeconds(),
    'q+': Math.floor((d.getMonth() + 3) / 3),
    S: d.getMilliseconds()
  }
  if (/(y+)/.test(_format)) _format = _format.replace(RegExp.$1, (d.getFullYear() + '').substr(4 - RegExp.$1.length))
  for (const k in o) if (new RegExp('(' + k + ')').test(_format)) _format = _format.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (('00' + o[k]).substr(('' + o[k]).length)))
  return _format
}

/**
 * 根据订单状态码返回订单状态
 * @param status_code
 * @returns {string}
 */
export function unixOrderStatus(status_code) {
  switch (status_code) {
    case 'NEW':
      return '新订单'
    case 'INTODB_ERROR':
      return '入库失败'
    case 'CONFIRM':
      return '已确认'
    case 'PAID_OFF':
      return '已付款'
    case 'SHIPPED':
      return '已发货'
    case 'ROG':
      return '已收货'
    case 'COMPLETE':
      return '已完成'
    case 'CANCELLED':
      return '已取消'
    case 'AFTER_SERVICE':
      return '售后中'
  }
}

/**
 * 13888888888 -> 138****8888
 * @param mobile
 * @returns {*}
 */
export function secrecyMobile(mobile) {
  mobile = String(mobile)
  if (!/\d{11}/.test(mobile)) {
    return mobile
  }
  return mobile.replace(/(\d{3})(\d{4})(\d{4})/, '$1****$3')
}

/**
 * 格式化货品的规格
 * @param spec_list
 * @returns {*}
 */
export function formatterSkuSpec(spec_list) {
  if (!spec_list || !spec_list.length) return ''
  return spec_list.map(spec => spec.spec_value).join(' - ')
}



/**
 * 解析售后状态
 * @param {Object} val
 */
export function parseServiceStatus(val) {
    switch (val) {
      case 'APPLY':
        return '售后服务申请成功，等待商家审核';
      case 'PASS':
        return '商家已审核，请提交退货物流信息(未收货取消订单不需要寄回商品)';
      case 'REFUSE':
        return '售后服务申请已被商家拒绝，如有疑问请及时联系商家';
      case 'FULL_COURIER':
        return '申请售后的商品已经寄出，等待商家收货';
      case 'STOCK_IN':
        return '商家已将售后商品入库';
      case 'WAIT_FOR_MANUAL':
        return '等待平台进行人工退款';
      case 'REFUNDING':
        return '商家退款中，请您耐心等待';
      case 'COMPLETED':
        return '售后服务已完成，感谢您的支持';
      case 'ERROR_EXCEPTION':
        return '系统生成新订单异常，等待商家手动创建新订单';
      case 'CLOSED':
        return '售后服务已关闭';
      default:
        return '';
    }
}

/**
 * 退款方式
 * @param {Object} val
 */
export function refundWayFilter(val) {
    switch (val) {
      case 'ACCOUNT':
        return '账户退款';
      case 'OFFLINE':
        return '线下退款';
      case 'ORIGINAL':
        return '原路退回';
      case 'BALANCE':
        return '预存款退款';
      default:
        return '其它';
    }
}

/**
 * 账号类型
 * @param {Object} val
 */
export function accountTypeFilter(val) {
    switch (val) {
      case 'WEIXINPAY':
        return '微信';
      case 'ALIPAY':
        return '支付宝';
      case 'BANKTRANSFER':
        return '银行卡';
      case 'BALANCE':
        return '预存款'
      default:
        return '其它';
    }
}

/**
 * 直播
 * @param {Object} val
 */
export function liveStatusFilter(val) {
    switch(val) {
        case '101': return '直播中'
        case '102': return '未开始'
        case '103': return '已结束'
        case '104': return '禁播'
        case '105': return '暂停中'
        case '106': return '异常'
        case '107': return '已过期'
    }
}