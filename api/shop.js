/**
 * 店铺相关API
 */

import request,{Method} from '../utils/request'

/**
 * 获取店铺列表
 * @param params
 */
export function getShopList(params) {
  return request.ajax({
    url: '/shops/list',
    method: Method.GET,
    loading: true,
    params
  })
}

/**
 * 会员初始化店铺信息
 */
export function initApplyShop() {
  return request.ajax({
    url: '/shops/apply',
    method: 'post',
    needToken: true
  })
}

/**
 * 获取店铺信息
 */
export function getApplyShopInfo() {
  return request.ajax({
    url: '/shops/apply',
    method: 'get',
    needToken: true
  })
}

/**
 * 会员申请开店步骤
 * @param step
 * @param params
 */
export function applyShopStep(step, params) {
  return request.ajax({
    url: `/shops/apply/step${step}`,
    method: 'put',
    needToken: true,
    params
  })
}

/**
 * 获取店铺基本信息
 * @param shop_id
 */
export function getShopBaseInfo(shop_id) {
  return request.ajax({
    url: `/shops/${shop_id}`,
    method: 'get'
  })
}

/**
 * 获取店铺幻灯片
 * @param shop_id
 */
export function getShopSildes(shop_id, client_type) {
  return request.ajax({
    url: `/shops/sildes/${shop_id}`,
    method: 'get',
    params: { client_type }
  })
}

/**
 * 获取店铺导航
 * @param shop_id
 */
export function getShopNav(shop_id) {
  return request.ajax({
    url: `/shops/navigations/${shop_id}`,
    method: 'get'
  })
}

/**
 * 获取店铺分类【分组】
 * @param shop_id
 */
export function getShopCategorys(shop_id) {
  return request.ajax({
    url: `/shops/cats/${shop_id}`,
    method: Method.GET
  })
}

/**
 * 收藏店铺
 * @param {Object} shop_id
 */
export function collectionShop(shop_id) {
  return request.ajax({
    url: `/members/collection/shop`,
    method: Method.POST,
    needToken: true,
    params: {
        shop_id: shop_id,
    }
  })
}


/**
 * 取消收藏店铺
 * @param {Object} shop_id
 */
export function canCalcollectionShop(shop_id) {
  return request.ajax({
    url: `/members/collection/shop/${shop_id}`,
    method: Method.DELETE,
    needToken: true,
    params: {
        shop_id,
    }
  })
}
