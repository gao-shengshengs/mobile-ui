/**
 * 公共API
 */
import request,{Method} from '../utils/request'
import { api } from '../config/config'
import {Keys} from '@/utils/cache.js';

const urlPrefix = api.base + '/base/'

/**
 * 获取验证方式
 */
export function getValidator() {
  return request.ajax({
    url: `${api.base}/base/validator`,
    method: Method.GET
  })
}
/**
 * 获取图片验证码URL
 * @param uuid
 * @param type
 * @returns {string}
 */
export function getValidateCodeUrl(uuid, type) {
  if (!uuid || !type) return ''
  return `${urlPrefix}captchas/${uuid}/${type}?r=${new Date().getTime()}`
}

/**
 * 获取站点设置
 */
export function getSiteData() {
  return request.ajax({
    url: `${urlPrefix}site-show`,
    method: Method.GET
  })
}
/**
 * 记录商品浏览量【用于统计】
 */
export function recordView(url) {
  return request.ajax({
    url: '/view',
    method: Method.GET,
    needToken: !!uni.getStorageSync(Keys.accessToken),
    params:{
      url,
      uuid: uni.getStorageSync(Keys.uuid)
    }
  })
}

/**
 * 获取base64图片
 * @param url
 */
export function getBase64(url) {
  return request.ajax({
    url: `${urlPrefix}net-image/base64`,
    method: Method.GET,
    params: {
      url
    }
  })
}