/**
 * 收货地址相关API
 */

import request,{Method} from '../utils/request'
import { api } from '../config/config'

/**
 * 获取收货地址列表
 * @returns {AxiosPromise}
 */
export function getAddressList() {
  return request.ajax({
    url: '/members/addresses',
    method: Method.GET,
    loading: true,
    needToken: true
  })
}

/**
 * 添加收货地址
 * @param params 地址参数
 * @returns {AxiosPromise}
 */
export function addAddress(params) {
  return request.ajax({
    url: '/members/address',
    method: Method.POST,
    loading: true,
    needToken: true,
    params
  })
}

/**
 * 编辑地址
 * @param id 地址ID
 * @param params 地址参数
 * @returns {AxiosPromise}
 */
export function editAddress(id, params) {
  return request.ajax({
    url: `/members/address/${id}`,
    method: Method.PUT,
    loading: true,
    needToken: true,
    params
  })
}

/**
 * 删除收货地址
 * @param id
 */
export function deleteAddress(id) {
  return request.ajax({
    url: `/members/address/${id}`,
    method: Method.DELETE,
    needToken: true
  })
}

/**
 * 设置默认地址
 * @param id
 */
export function setDefaultAddress(id) {
  return request.ajax({
    url: `/members/address/${id}/default`,
    method: Method.PUT,
    needToken: true
  })
}

/**
 * 获取某个地址详情
 * @param id
 */
export function getAddressDetail(id) {
  return request.ajax({
    url: `/members/address/${id}`,
    method: Method.GET,
    loading: true,
    needToken: true
  })
}

/**
 * 获取地区列表
 * @param id
 */
export function getAreas(id) {
  const _api = `/base/regions/@id/children`
  return request.ajax({
    url: _api.replace('@id', id),
    method: Method.GET
  })
}

/**
 * 智能识别地址是否开启
 * @returns 
 */
export function getExpressStatus() {
  return request.ajax({
    url: '/express/open',
    method: Method.GET,
    needToken: true
  })
}

/**
 * 地址识别分析
 * @returns 
 */
export function getAddressAnalysis(params) {
  return request.ajax({
    url: '/express/analysis',
    method: Method.GET,
    needToken: true,
		params
  })
}