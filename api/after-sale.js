/**
 * 申请售后相关API
 */

import request,{Method} from '../utils/request'

/**
 * 获取售后列表
 * @param params
 * @returns {AxiosPromise}
 */
export function getAfterSale(params) {
  return request.ajax({
    url: 'after-sales',
    method: Method.GET,
    loading: true,
    needToken: true,
    params
  })
}


/**
 * 获取售后详情
 * @param sn 订单编号
 * @returns {AxiosPromise}
 */
export function getAfterSaleDetail(sn) {
  return request.ajax({
    url: `/after-sales/detail/${sn}`,
    method: Method.GET,
    needToken: true
  })
}

/**
 * 申请退款
 * @param params
 */
export function applyAfterSaleMoney(params) {
  return request.ajax({
    url: '/after-sales/apply/cancel/order',
    method: 'post',
    needToken: true ,
    params
  })
}

/**
 * 申请退货
 * @param params
 */
export function applyAfterSaleGoods(params) {
  return request.ajax({
    url: '/after-sales/apply/return/goods',
    method: Method.POST,
    needToken: true,
    data: params,
  })
}

/**
 * 申请换货
 * @param params
 */
export function applyAfterSaleChange(params) {
  return request.ajax({
    url: '/after-sales/apply/change/goods',
    method: Method.POST,
    needToken: true ,
    data: params
  })
}


/**
 * 申请补发
 * @param params
 */
export function applyAfterSaleReplace(params) {
  return request.ajax({
    url: '/after-sales/apply/replace/goods',
    method: Method.POST,
    needToken: true ,
    data: params
  })
}

/**
 * 申请取消订单 (已付款 && 未发货)
 * @param params
 */
export function applyAfterSaleCancel(params) {
  return request.ajax({
    url: '/after-sales/apply/cancel/order',
    method: Method.POST,
    needToken: true ,
    params
  })
}

/**
 * 获取售后申请数据
 * @param order_sn
 * @param sku_id
 */
export function getAfterSaleData(order_sn, sku_id) {
  return request.ajax({
    url: `after-sales/apply/${order_sn}/${sku_id}`,
    method: Method.GET,
    needToken: true
  });
}


/**
 *
 保存用户填写的物流信息
 * @param params
 */
export function saveShip(params) {
  return request.ajax({
    url: `/after-sales/apply/ship/${params.service_sn}`,
    method: Method.POST,
    needToken: true,
    params,
  });
}


/**
 * 获取售后服务操作日志信息
 * @param service_sn 售后服务单编号
 */
export function getServiceLogs(service_sn) {
  return request.ajax({
    url: `/after-sales/log/${service_sn}`,
    method: Method.GET,
    needToken: true
  })
}